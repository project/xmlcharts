<?php

/**
 * @file
 * Drupal Module: XML Charts installation file.
 *
 * XMLCharts is a module that consumes,
 * caches and provides blocks to display
 * industrial and precious metal prices
 * from XMLCharts Free XML Price Feeds.
 *
 * author: Andrew Wasson <https://drupal.org/user/127091>
 */

/**
 * Implements hook_install().
 */
function xmlcharts_install() {
  $xml_charts_data       = new XMLChartsData();
  $url_precious_metals   = $xml_charts_data->urlPreciousMetals;
  $url_industrial_metals = $xml_charts_data->urlIndustrialMetals;
  $options               = $xml_charts_data->options;

  watchdog('xml_charts', 'XML Charts installed on: @date', array('@date' => format_date(REQUEST_TIME, 0)));

  // $xml_precious_metals
  if ($cache = cache_get('xml_charts_precious_metals')) {
    $xml_precious_metals = $cache->data;
    watchdog('xml_charts', 'XML Charts will use cached data for Precious Metals until next update');
  }
  else {
    // Pull the data in and save to the cache.
    $xml_precious_metals = drupal_http_request($url_precious_metals, $options);
    cache_set('xml_charts_precious_metals', $xml_precious_metals, 'cache');
    watchdog('xml_charts', 'Precious Metals data acquired and cached from XML Charts webservice');
  }

  // $xml_industrial_metals
  if ($cache = cache_get('xml_charts_industrial_metals')) {
    $xml_industrial_metals = $cache->data;
    watchdog('xml_charts', 'XML Charts will use cached data for Industrial Metals until next update');
  }
  else {
    // Pull the data in and save to the cache.
    $xml_industrial_metals = drupal_http_request($url_industrial_metals, $options);
    cache_set('xml_charts_industrial_metals', $xml_industrial_metals, 'cache');
    watchdog('xml_charts', 'Industrial Metals data acquired and cached from XML Charts webservice');
  }
}

/**
 * Implements hook_uninstall().
 */
function xmlcharts_uninstall() {
  // Use db_delete() + cache_clear_all() rather than 36 calls to variable_del().
  db_delete('variable')->condition('name', db_like('xmlcharts_') . '%', 'LIKE')->execute();
  cache_clear_all('variables', 'cache_bootstrap');
}
